## Melissa Wen's qualification exam manuscript

Text presented to the Institute of Mathematics and Statistics of the University
of São Paulo for the Master of Science qualifying examination.

**Title:** What Happens When The Bazaar Grows:
*A comprehensive study on the contemporary FLOSS development*  
**Author:** Melissa Shihfan Ribeiro Wen  
**Date:** July 19th at 8 a.m. UTC -3 / Brazil  
**Place:** Room 249 - Building A of the IME/University of São Paulo  
**Program:** Computer Science  
**Advisor:** Prof. Dr. Fabio Kon  
**Coadvisor:** Prof. Dr. Paulo Roberto Miranda Meirelles  

### Cite This

#### Plain Text

Wen, Melissa (2019), "What Happens When The Bazaar Grows: A comprehensive study
on the transformations of the FLOSS development model", Masters Thesis
Qualifying Exam Report at Institute of Mathematics and Statistics of the
University of São Paulo. Retrieved from https://gitlab.com/ccsl-usp/floss-research/melissa-wen-quali-manuscript/wen-manuscript-quali.pdf


#### Bibtex

```
@unpublished{Wen2019,
  author = {Wen, Melissa},
  institution = {Institute of Mathematics and Statistics of the University of S{\~a}o Paulo},
  title = {What Happens When The Bazaar Grows: A comprehensive study on the transformations of the FLOSS development model},
  howpublished = {Masters Thesis Qualifying Exam Report},
  month = jul,
  year = 2019,
  url = {https://gitlab.com/ccsl-usp/floss-research/melissa-wen-quali-manuscript/wen-manuscript-quali.pdf}
}
```
