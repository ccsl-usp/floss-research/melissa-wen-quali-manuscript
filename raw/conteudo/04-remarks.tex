\chapter{Preliminary Considerations and Work Plan}
\label{cap:remarks}

After completing the mandatory course credits for the MSc in computer
science at USP and also publishing some results of work done in the field of
FLOSS ecosystems (Appendix \ref{att:firstyear}), this research has advanced on three different fronts:
(1) Secondary Studies; (2) Participant Observation; and
(3) Development of a FLOSS Data Collection Platform.
In this chapter, we will show the current stage of each of these fronts and
the work schedule for completing the research.

\section{Current Stage}
\label{sec:currentstage}

As described in Section \ref{sec:researchdesign}, this research is divided
into three phases. Phase I is a warm-up phase, deepening the theme and
getting familiar with the multiple methods adopted in the research design. Phase
II focuses on state-of-the-art building and FLOSS development practice from the
software engineering point of view. Phase III focuses on the ethnographic study
of the Linux kernel project, where we will triangulate findings of phase II
with those obtained through participant and non-participant observation and
mining of community production by a platform under development.

Phase I was addressed by reading different kinds of publications on FLOSS
development and selection of baseline papers; introduction to the community in
which participant observation will be carried out and the definition of
requirements, and architectural design of the platform for data collection of
FLOSS community interactions. We detail each of these activities below.

\subsection{Secondary Study}
\label{sec:secstudy}

First, we selected a few papers about FLOSS development and Linux project
development by experts suggestion. Next, we identified papers that fit this
research context, and we searched for other relevant publications of their
authors. Finally, a set of 10 articles were chosen as key papers to support
phase II in the conduction of a multivocal literature review.

\begin{table}[]
\resizebox{\textwidth}{!}{\begin{tabular}{  m{9cm} m{5cm} m{2cm}  }
\hline
Title & Autor & Year \\ \hline
Lessons Learned from a Decade of FLOSS Data Collection & Crowston, K. and Squire, M. & 2017 \\ \hline
Open Source Technology Development & Kevin Crowston & 2015 \\ \hline
A systematic literature review on the barriers faced by newcomers to open source software projects & Steinmacher, I. Silva, M. Gerosa, M. and Redmiles, D. & 2015 \\ \hline
Peer Review on Open-Source Software Projects: Parameters, Statistical Models, and Theory & Rigby, P. and German, D. and Cowen, L. and Storey, M. & 2014 \\ \hline
A Systematic Review of Research on Open Source Software in Commercial Software Product Development & Host, M. and Oruvcevic-Alagic, A. & 2011 \\ \hline
Free and Open Source Software Development and Research: Opportunities for Software Engineering & Kon F. and Meirelles P. and Lago N. and Terceiro A. and Chavez C and Mendonca M. & 2011 \\ \hline
Free/Libre Open Source Software Development: What We Know and What We Do Not Know & Crowston, K. and Wei, K. and Howison, J. and Wiggins, A. & 2010 \\ \hline
Free/Open Source Software Development: Recent Research Results and Emerging Opportunities & Scacchi, W. & 2007 \\ \hline
Role Migration and Advancement Processes in OSSD Projects: A Comparative Case Study & Jensen, C. and Scacchi, W. & 2007 \\ \hline
A Critical Review of Software Engineering Research on Open Source Software Development & Østerlie T., Jaccheri L. & 2007 \\ \hline
\end{tabular}}
\caption{List of publications considered in warm-up phase}
\label{tab:listpapers}
\end{table}

Table \ref{tab:listpapers} presents the selected key
papers.
We extracted a set of statements from the studies mentioned above to guide the design of the grey
literature review protocol, and it will also guide the systematic review protocol:
\begin{itemize}
	\item \citet{Osterlie2007}: Software engineering studies have a black-and-white view
		of the FLOSS phenomenon, generalizing, homogenizing and
		distancing itself from FLOSS as a software development model.
	\item \citet{Fitzgerald2006}:FLOSS has left its original bazaar and free-software models and
		has become a more popular and commercial product where
		the interest of large corporations directly interfere in
		its development process and decision-making process.
        \item \citet{Crowston2012}: FLOSS can be characterized as a public product produced by private initiative with private and public development practices.
	\item \citet{Rigby2014}: Currently, the development of the Linux project is less
		bazaar-like and resembles more a dictatorship, where the dictator
		interacts with maintainers through a chain-of-trust.
	\item \citet{Scacchi2007}:Software
		Informalism is a rich source of data produced by practitioners
		and little explored by software engineering studies.
	\item \citet{Rigby2014}: About 40\% of the patches sent to Linux project are ignored
		and never included in the kernel source tree.
\end{itemize}

Now, we are planning the GLR. To enable the systematic
conduction of the review, we are designing a GLR protocol based on the systematic
review process and GLR guidelines proposed by recent software engineering
studies.

\subsection{Participant Observation in the Linux IIO Subsystem}

In participant observation, becoming a member of the community to be studied is
the observer's first task. In the case of the community around the Linux
subsystem development, in addition to the very complexity of the code of this
project, the observer must correctly configure the development environment. She
also needs to learn the community rules to send contributions and dialogue
with other project members using a mailing list.

As a task for immersion in the community, we have chosen the development of
solutions to move a specific device driver from the staging phase to the stable version.
The driver selected was the capacitance-to-digital
converter AD7150\footnote{\url{https://www.analog.com/en/products/ad7150.html}},
designed by Analog Device Inc.

Initially, I conducted a training phase on how to develop device drivers and
send patches to the IIO subsystem. For this, I executed a series of tutorials
and also interacted with FLUSP (FLOSS at USP)\footnote{\url{https://flusp.ime.usp.br}},
an extension group of the University of São Paulo (USP), created to foster student
participation at FLOSS projects.
To become part of the group of community developers and understand the
contribution workflow (submission, revision, acceptance, and merge), I, as a
participant observer, developed a total of eight patches to the community that
is currently under discussion. A summary of these contributions is presented in
Appendix \ref{att:contribIIO}. The ethnographer must also take note of her
observations, interpretations, and reflections, so I am in parallel, writing a
contribution diary where I describe each cycle of contribution, discovery,
and questions.

\subsection{Development of a FLOSS Data Collection Platform}

Most FLOSS projects have a standard set of information management tools~\citep{Fogel2005, Goldman2005}:
\begin{enumerate}
\item \textit{Website} publicly spreads project information unilaterally and centrally, providing an administrative interface to the other tools.
\item \textit{Mailing Lists} are the most active communications forum and the medium of record.
\item \textit{Version Control System} manages code changes and enables everyone to watch the code.
\item \textit{Issue tracker} enables tracking of bugs, tasks, releases, and new features, improving the group task coordination. 
\item \textit{IRC/ Real-time chat} is a place for lightweight discussion and quick question and answer. 
\end{enumerate}

From this standard, the research questions and the statements obtained in the key papers selection phase~\ref{sec:secstudy}, we
have defined a set of requirements for the development of the MsFLOSS platform,
Appendix \ref{ap:msflossreq}. We detailed and discussed these requirements with students of
the ``Advanced Topics in Object-oriented Programming'' course, taught by Prof. Fabio Kon, for undergraduate and graduate students. In
this course, we are acting as customers for an initial version of the
platform. 

The basic requirements of the platform are:
\begin{itemize}
\item Store and retrieve project community activities.
\item Monitor of updates in four community interaction means  (Mailing List, IRC channel, Issue Tracker, and Commits Tree) of a given project. 
\item Collect and mining of public data in a short time.
\item Register projects and addresses of the networks to be monitored. 
\item Visualize evolutionary project graphs, quantitative indexes and natural language processing data.
\end{itemize}

This platform enables any user can register a FLOSS project and set which interaction environment are available for this project. Each environment has a set of information to be collected and preprocessed, as described in Appendix~\ref{ap: msflossreq}. The platform must be able to pre-process the activities and provide, in general, the following information:
\begin{itemize}
\item The profile of the author (s) of an activity;
\item The profile of the recipient (s) of a given activity (if any);
\item The profile of the respondent (s) to a given activity (if any);
\item The level of participation of each active member;
\item The way a particular member communicates with the others;
\item Determine more active members: sending contributions, answering questions, reviewing code, etc.;
\item The number of reviewers and the number of comments of a given contribution;
\item What kind of contribution and which flags were used;
\item In which part of a patch-contribution the comments usually focus on;
\item The number of versions of a patch developed until the maintainer accepts it;
\item How long it takes between the discovery of a bug and the incorporation of its resolution into the code.
\end{itemize}

The student-developers proposed a microservice architecture, as shown in
Figure \ref{fig:msfloss} of Appendix \ref{ap:msflossreq}, where, for each interaction space of a project, a
specialized robot is monitoring and capturing activities, performing a small
pre-processing of the data and storing it in an independent database. The
addresses for the spaces are provided by the user who indicates which environment will be monitored when registering a
particular project. After the
project registration, the referred project page will display charts and
meaningful indexes. The data collector will perform the intermediation between
the microservices, a kind of proxy that interacts with services through APIs.


\newpage

\section{Schedule}
\label{sec:schedule}

%%%%%%% Cronograma %%%%%%%

\begin{figure}
  \centering

  \begin{ganttchart}{2019-07}{2020-07}
    \gantttitlecalendar{year,month=shortname} \ganttnewline
    \ganttgroup[progress=15]{Multivocal Literature Review}{2019-07}{2019-12} \ganttnewline
    \ganttbar[progress=20]{Grey Literature Review}{2019-07}{2019-09} \ganttnewline
    \ganttbar[progress=0]{Systematic Literature Review}{2019-10}{2019-12} \ganttnewline
    \ganttbar[progress=0]{Coding}{2019-10}{2019-12} \ganttnewline
    \ganttgroup[progress=0]{Conference Paper Writting}{2019-08}{2019-10} \ganttnewline
    \ganttgroup[progress=10]{Participant and Non-Participant Observation}{2019-07}{2020-03} \ganttnewline
    \ganttbar[progress=100]{Contributions Warm-up}{2019-07}{2019-07} \ganttnewline
    \ganttbar[progress=0]{Design Data Collection}{2019-10}{2019-10} \ganttnewline
    \ganttbar[progress=0]{Collect Data}{2019-11}{2020-01} \ganttnewline
    \ganttbar[progress=0]{Design Data Analysis}{2020-01}{2020-01} \ganttnewline
    \ganttbar[progress=0]{Perform Data Analysis}{2020-02}{2020-03} \ganttnewline 
    \ganttgroup[progress=20]{FLOSS Data Collection}{2019-07}{2020-02} \ganttnewline
    \ganttbar[progress=25]{Platform Development}{2019-07}{2019-10} \ganttnewline
    \ganttbar[progress=0]{Data Mining}{2019-10}{2019-12} \ganttnewline
    \ganttbar[progress=0]{Data Analysis}{2019-12}{2020-02} \ganttnewline
   \ganttgroup[progress=0]{Compare Findings}{2020-02}{2020-04} \ganttnewline
    \ganttgroup[progress=0]{Conference Paper Writting}{2020-04}{2020-06} \ganttnewline
    \ganttgroup[progress=0]{Thesis Writting}{2020-05}{2020-07} \ganttnewline
    \ganttmilestone{Submission}{2020-07}
  \end{ganttchart}

  \caption{Proposed Schedule.\label{fig:gantt}}
\end{figure}

\subsection{Multivocal Literature Review}

We will conduct this step in the following sequence: (1) gray literature review
(non-formal publications); (2) coding of GLR evidence; (3) systematic review of
the literature (formal papers); (4) coding of the SLR evidence;

\subsection{Participant Observation}

This step is subdivided into two data collection fronts, where the first
consists of the data collected by an observer participating directly in the IIO
subsystem community, and the second collects data by observing the online
activities of other FLOSS developers of the same or another subsystem. For
this, we will have a round of design and test of the field research and a
second round with the performance of already refined field research.

\subsection{Platform Development}

This stage consists of the development of a platform to collect community
interaction data (the MsFLOSS); followed by data mining of the community in
which the participant observation was performed; and, finally, pre-analysis
guided by the attributes and evidence of previous literature reviews.

\subsection{Cross-Analysis of Findings}

To conclude this work, we will cross-analyze the results found in the previous
three steps, that is, a triangulation of the pieces of evidence.

%%%%%
