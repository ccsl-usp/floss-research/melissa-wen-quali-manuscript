%% ------------------------------------------------------------------------- %%
\chapter{Research Methods}
\label{cap:researchMethods}

According to \citet{Osterlie2007}, software engineering literature has presented
little about what is performed by practitioners; what activities they do and
how often they happen. To fill the gap between practice and theory,
researchers should move from a research-and-transfer model to
industry-as-laboratory approach. With this concern in mind, this work proposes a
multiple-method investigation, adopting multivocal systematic review,
ethnographic case study, online participant and non-participant observation,
data mining and techniques from grounded theory to update the perception of
academia and practitioners on the characteristics of the ecosystem that inspired the
FLOSS development model.

In this chapter, we introduce the research methods that guide this
investigation. We discuss the advantages and challenges of each of them. We
also present guidelines for the use of each approach in software engineering
investigations.

%% ------------------------------------------------------------------------- %%
\section{Research Strategies}
\label{sec:researchstrategy}

Searching for information only in formal
references does not fit well into the universe of FLOSS. 
%\todo{Não manjo desses paranauê, mas acho que engenheiros de soft querem que tudo seja feito formalmente (o que, como bem sabemos, tem lá seus problemas). Mas o que você está dizendo aqui é outra coisa: que a \textit{pesquisa} em engenharia de soft sempre se baseia apenas em documentação formal. Se isso é verdade (ou na medida em que isso é verdade), o problema não é só com FLOSS; a análise de qualquer projeto que não seja super-formal vai dar merda}.
A FLOSS ecosystem usually
has a community around its project, organically producing materials that
describe how, when, and why a particular change occurred in the source code or
the development flow. This production provides activity logs, documentation of
debates, and reflections present in the developer's day-to-day.
\citet{Scacchi2007} define as \textit{Software Informalism} the information resources and artifacts used by
participants to describe, prescribe, or proscribe what happens in a FLOSS
project. They are informal, comparatively easy-to-use and publicly accessible
narratives such as source code, artifacts, online repositories, with
substantial size, diversity, and complexity not previously available globally.
These materials can be complemented by online and
non-academic publications to broaden the academic understanding of how FLOSS project
developers interpret the development model of which they are part.

Besides the specificity in the production of informal narratives published
openly on the Internet, FLOSS development studies must also consider the
pillars that sustain the community of developers for a given project. Trust
and responsibility among project participants are invisible social control
resources that support the open and complex development of
FLOSS~\citep{Scacchi2007}. Describing
how contributors interpret the organization and workflow of a project requires
research methods capable of explaining the rationalities from an insider's point of
view and puts the social, cooperative, and human aspects of software
engineering practice at the center~\citep{Sharp2016}. Finally, Empirical
studies of FLOSS are growing and expanding the scope of what we can observe,
analyze and learn about these software systems and their development from the
joining of traditional research methods to new techniques for mining
repositories and analysis of the socio-technical processes and environments
which support FLOSS projects~\citep{Scacchi2010}.

Considering the research questions enunciated in Section \ref{sec:researchdesign}, we
have a mix of questions types. RQ1 (\textit{How do software engineering studies and FLOSS development communities
define the current models of FLOSS development and their ties to the
Linux model?}) is both exploratory and descriptive.

For answering RQ1, we
will conduct a multivocal systematic review to construct a comprehensive, up-to-date
view of FLOSS development models considering formal, online and non-academic
publications, and FLOSS artifacts. After, we will analysis this set of data using
coding techniques to identify common attributes used by them for a development
model characterization. 

RQ2 (\textit{How to confirm the characteristics used to describe a
particular FLOSS project development, exploring its different means of social
interaction? How to visualize socio-technical aspects that impact the
development process?}) is exploratory and evaluative, since we are interested in evaluating how
well the characteristics described by the academic and non-academic literature fit
the evidence from the practice. It also has an ethnographic nature because it concerns itself with the social
and behavioral aspects of groups of people in the means of interactions.

Community and
project diversity constrain the statistical generalization of results found in
case studies of FLOSS projects. However, the ethnographic approach communicates
a detailed picture that enables analytical generalizations. Thus, we designed an
online ethnographic case study on the Linux kernel project to capture the
``what'' and ``why'' of practices from different sources of data. On one front, an
open platform will be developed to collect and mine repository data,
mailing-list, and commits tree. On another front, we will collect qualitative
data through online participant observation in a Linux subsystem and
non-participant observation of informants in other subsystems. Finally, we will
compare the findings from the mixed type of data analysis with those found in
the literature review to illustrate in detail how to find gaps and build bridges
on theory and practice understanding of FLOSS development.

%% ------------------------------------------------------------------------- %%
\subsection{Multivocal Literature Review}
\label{sec:multivocal}

A Multivocal Literature Review (MRL) is a form of systematic literature review that
includes inputs from academic peer-reviewed papers and sources from the Grey
Literature~\citep{Garousi2018}. It aims to provide summaries both the
state-of-art and –practice in a given area.

\subsubsection{Systematic Literature Review}
\label{subsec:slr}

Systematic Literature Review (SLR) is a research methodology used to
synthesize and evaluate the available evidence on a focused
topic~\citep{Biolchini2007}. It follows a rigorous methodology of research results
to support the development of evidence-based guidelines for
practitioners~\citep{Kitchenham2009}. The evidence knowledge allows researchers to
capture gaps and concept conflicts to be addressed by new studies in the
area~\citep{Melo2013}.

In contrast to ad-hoc literature reviews, a systematic review follows a
sequence of well-defined, strict steps following a protocol planned before
execution. The whole SLR process must be documented, explicitly defining
methodological steps, collection strategies, focus, research question, and also
intermediate results. 

According to \cite{Cochrane2008}, a systematic review should have the following
characteristics:

\begin{itemize}
\item A set of clearly stated goals with predetermined inclusion and exclusion
criteria for studies;
\item An explicit and reproducible methodology;
\item The use of systematic research to reach all studies that fit the
eligibility criteria;
\item An evaluation of the validity of the findings of the selected studies,
including the risk of bias; and
\item A systematic and synthesized presentation of the characteristics and
results of the selected studies.
\end{itemize}

Although SLR has become more prevalent in empirical software engineering, many
of these studies use medical standards to guide review~\citep{Kitchenham2013}.
\cite{Budgen2006} proposed a three-phase review process for Software
Engineering: (1) planning the review; (2) conducting the review; (3) reporting
the outcomes from the review. \cite{Biolchini2007} developed a similar
three-phase template (Annex~\ref{ann:slrtemp}) based on protocols and systematic review
guidelines in the medical field to guide the planning and execution of SLR in
Software Engineering. This template handles each phase of the SLR process as
detailed below:

\begin{itemize}
\item \textit{Review Planning:} defines question focus; question quality and
amplitude (problem, question, keywords, PICO components, and others semantics
specificity); Source Selection (criteria definition, studies languages, sources
selection, source and references evaluation); Studies Selection (inclusion and
exclusion criteria; studies types; procedures for selection).
\item \textit{Review Execution:} Selection Execution (reports primary studies
selection); Information Extraction (describes extraction criteria, results, and
resolution of divergences among reviewers).
\item \textit{Result Analysis:} summarizes and analyzes results using
statistical methods.
\end{itemize} 

\textbf{SLR Advantages and Challenges in Software Engineering}

According to \cite{Kitchenham2007}, the main advantages of SLR are:

\begin{itemize}
\item Low probability of bias due to the use of a well-defined methodology
(although not exempted to the prejudices of primary studies);
\item Informs the effects of a phenomenon across a wide range of settings and
empirical methods;
\item In the case of quantitative studies, SLR enables the combination of data
using meta-analysis techniques.
\end{itemize}

The SLR major challenge is that it requires more effort than a non-systematic review.
Some additional problems arise in the area of software engineering,
for example, digital libraries lack mechanisms to perform complex
queries, papers omit information, and their abstracts are
poor~\citep{Kitchenham2013}.

\subsubsection{Grey Literature Review}
\label{subsec:glr}

Grey Literature Review (GLR) is any source of information
produced from academia, industry, business, and government, published in print
or electronic formats, but commercial publishers~\citep{Rothstein2005} do not
control it. It is composed of sources of data not found in the formal
literature such as blogs, videos, white papers, and web-pages. It is also a
great forum to disseminate more critic investigations and negative of null
results, reducing bias and visualizing evidence more balanced and
accurate~\citep{Paez2017}.

Combining
systematic review protocols with GLR helps to shed light on huge
industrial adopted areas still not mapped by software engineering
studies~\citep{Soldani2018}. However, the use of GLR also has
challenges. Although institutions recommend the inclusion of this literature, a
precise method for implementation is not available. Thus, GLR still faces
difficulties due to the large volume of information and the lack of indexing
standards and vocabulary~\citep{Godin2015}.

\cite{Godin2015} present an application of systematic review methods in GLR,
where information sources include GLR databases, customized Google search
engines, targeted websites, and consults with contact experts. They use title and source organization of
documents for eligibility assessment and study selection. Finally, they search
for a pre-defined a set of data on each study selected.

\subsubsection{Conducting a Multivocal Literature Review}
\label{subsec:mlr}

A multivocal literature review could improve the relevance of software
engineering research accepting and analyzing input from practitioner
literature. \citet{Garousi2018} present guidelines to include GLR in SLR and
conduct a multivocal literature review. The proposed MRL
process is composed of five phases: 

\begin{itemize}
\item \textbf{Search process:} 
\begin{itemize}
\item Defining search string and/or using snowballing techniques.
\item Where to search: search formally-published literature via broad-coverage
abstract databases and full-text databases; search GLR via a general
web search engine, specialized databases, and websites, contacting individuals
directly or via social media, and/or reference lists and backlinks, and perform
an informal pre-search to find different synonyms for specific topics.
\item Stopping criteria: theoretical saturation; effort bounded; evidence
exhaustion;
\end{itemize}
\item \textbf{Source selection:} combine inclusion and exclusion criteria for
GLR with quality assessment criteria; in the source selection
process of an MLR, one should ensure a coordinated integration of the source
selection processes for GLR and formal literature.
\item \textbf{Study quality assessment:} apply and adapt the criteria authority
of the producer, methodology, objectivity, date, novelty, impact, as well as
outlet control, for study quality assessment of GL.
\item \textbf{Data extraction:} design data extraction forms; use systematic
procedures and logistics, such as maintain ``traceability'' links, and extract and
record as much quantitative/qualitative data as needed to sufficiently address
each research question; 
\item \textbf{Data synthesis:} many GLR sources are suitable for qualitative
coding and synthesis; argumentation theory can be beneficial for data
synthesis; quantitative analysis is possible on GLR databases such as
StackOverflow.
\end{itemize}

Using secondary data is not a research method widely adopted in research on
FLOSS development~\citep{Crowston2012}. Moreover, although the SLR is a
well-established investigative method, with steps and restrictions already
defined in several fields of study, MLR, in turn, is an emerging technique,
especially in the area of software engineering. Its conduction still requires
adaptations for the variety of publication types on a particular topic under
investigation. To perform an MLR, the reviewer should deal with the lack of
robust search engines (developing customized search mechanisms), the
diversity of the size, quality and structure of the retrieved documents, as
well as the challenge on textual and natural language analysis.

\textbf{Coding}

Qualitative data are those in which information is expressed through figures
or words, whereas categories or numbers represent quantitative data~\citep{Seaman1999}. An
approach to extracting quantitative variable's values from qualitative data
is the process of \textit{coding}. Coding makes possible getting more
precise and reliable quantitative data and allows the quantification of
subjective information to perform some quantitative or statistical analysis.
To avoid loss of information in this transformation, the researcher needs to
be aware of the variations of terminology in the description of the
phenomena, the different ratings for the same subject and the margin of
reliability between them.

Three processes describe the coding analysis~\citep{Edmonds2013}:
\begin{enumerate}
\item \textit{Open coding} identifies categories of information about the phenomenon being examined.
\item \textit{Axial coding} involves taking each one of the categories from the open coding and exploring it as a core phenomenon.
\item \textit{Selective coding} systematically relates the core category with other categories. 
\end{enumerate}

%% ------------------------------------------------------------------------- %%
\subsection{Ethnographic Case Study}
\label{sec:ethnography}

Ethnography is a research method designed to describe and analyze the social life and
culture of a specific social system~\citep{Edmonds2013}. The central tenet of this approach is to
understand values, beliefs, or ideas shared for a group under study from the
members' point of view~\citep{Sharp2016}. For this, the ethnographer needs to become a member of
the group, observing in detail what people actually do and learning their
language, social norms, rules, and artifacts.

In software engineering, ethnography is an appropriate method of
research when one wants to understand people and culture and the social and
work practices associated with it~\citep{Sharp2016}. It can explain, from an empathic
perspective, the logic of practice from insiders' view and, thus, capture what
practitioners do and why they do. Considering the behavior of people an
integral part of software development and maintenance~\citep{Seaman2008},
ethnography in software engineering can strengthen investigations of social
and human aspects in the software development process since the significance
of these aspects of software practice is already well-established.

The ethnographic case study focuses on the examination of a real case in some
cultural group, bounded by time, place, and environment~\citep{Edmonds2013}. Its design is
characterized by the intensive and holistic description and analysis of a
particular social reality and is best suited for investigations interested in
exploring the activities of a group rather than shared patterns of group
behavior. Ethnography imposes an orientation of the investigative view on the
symbols, interpretations, beliefs, and values which integrate the
socio-cultural dimension of the dynamics of a community~\citep{Sarmento2011}. In this way, an
ethnographic case study must present an investigative design that employs
convergent methods with such orientation. \cite{Sharp2016} defines four main
characteristics of ethnographic research in empirical software engineering:

\begin{itemize}
\item \textbf{The members' point of view:} focus on the informants' point of
view, understanding what is, or is not, important, and painful for the
informant.
\item \textbf{The ordinary detail of life:} collect several types of data about
different aspects of their informants' work and keep ``open'' for new
possibilities. 
\item \textbf{The analytical stance:} provide an analysis of the results
explaining how this evidence is, or is not, relevant for a particular purpose.
\item \textbf{Production of ``thick descriptions'' for academic accountability:}
provide results well rooted in meaningful aspects.  Comparing and contrasting
data, their aggregation, and ordering it in logical sequences to structure the
knowledge.
\end{itemize}

The design of an ethnographic study must also consider five dimensions:
(1) participant or non-participant observation; (2) the duration of the field
study; (3) space and location; (4) the use of theoretical underpinnings;
(5) the ethnographers’ intent in undertaking the study.

\subsubsection{Data Collection Methods} 

Three methods of data collection are mostly used in ethnographic research:
participant observation, interviews, and documentation analysis~\citep{Sarmento2011}.
In this work, in addition to the evidence gathered by data analysis of the MLR,
we collect data of a specific case of FLOSS project by the participant and
non-participant observation and the examination of the community-produced
documents.

\textbf{Participant and Non-Participant Observation} 

Data from direct observation contrasts information obtained through interviews
and questionnaires. The reason for the differences is that the human does not
always do (or will do) what he says he has done, especially when it involves
his reputation. In this case, the participant observation method makes it
possible to explain the meaning of the experiences of the observed fact through
the experience of the observer of the fact~\citep{robson_realresearch} and allow
the informant judges what is important rather than what he thinks is important.

An ethnographer spends her time working, discussing, participating, living,
that is, interacting with informants. She becomes a member of the observed
group, interpreting what is happening around her. Hence, in addition to
sensitivity, the observer needs the personal skills necessary to conduct
participant observation. In a FLOSS community, the software engineer has the
facility to assume the role of participant observer and to provide valuable
information and practical consequences for the practice of software. Also,
informants who are of the same culture and living in similar conditions
generally reduce the time required to complete an ethnographic study~\citep{Sharp2016}.

According to \cite{Seaman2008}, a considerable part of software development work is not
documented because it occurs within the developer's mind. Software developers
find it easier to reveal the processes present in their thoughts when
communicating with other software developers, which makes this communication
a valuable opportunity to observe the development process. In this context, a
method for data collection is the continuous observation of a developer,
recording all interactions with the other members of the group through notes
or journals. This journal should be kept confidential throughout the study to
preserve the observer's total freedom of writing. Each note should include the
location, time, and participants of the observation, the discussions that took
place, the events that occurred during the observation, and the tone and
feelings involved in the interactions. As the observer becomes more familiar
with the group studied, the notes gain a wealth of detail.

\textbf{Developing a FLOSS Data Collection Platform} 

In ethnography, the analysis of community documents commonly complements the
data collected in the observation. FLOSS community produces organically
documentation regard its development activities, debates, decision taken, and
project architecture through mailing lists, IRC channels, issue trackers, and
also repository version control.
 
Supporting FLOSS project studies through the collection of different project
data is part of the development of tools such as
FLOSSmole\footnote{\url{https://flossmole.org}}. This tool allows the
collection,  storage, and sharing of various data from FLOSS projects, being
possible for each type of stored data, define a target project and perform
different  analyzes~\citep{Howison2005}. Focused on project repository data
visualization,  OpenHub\footnote{\url{https://www.openhub.net}}, formerly
Ohloh, is a large-scale index of open-source software projects~\citep{Bruntink2015}
to discover, evaluate,  track, and compare source code and projects. 

To support community data analysis, we are developing a platform for FLOSS
projects data collection, the
MsFLOSS\footnote{\url{https://gitlab.com/flusp/msfloss/msfloss}}. The platform
differs from others by almost instantaneously collecting data from different
socio-technical environments, where stakeholders typically interact in public.
These environments include mailing-lists, IRC channels, issue trackers, and git
repositories. The data mining and natural language processing will provide both
qualitative and quantitative interaction information in a way to validate and
evaluate findings and also filling possible gaps. 

\textbf{Triangulation of Data}

Crossing information obtained from different sources, different types of data
or different methods of collection and analysis allows performing a Triangulation~\citep{Seaman1999}. It is
considered the most powerful method of ``confirming'' the validity of conclusions. In the
case study, triangulation is important to prevent the unilaterality in a piece
of observation, a testimonial, or a document could compromise the understanding
of reality~\citep{Sarmento2011}.

Different kinds of measurements provide repeated verification, however,
triangulation gives reliability rather then validity information. This method also
reveals inconsistency or even direct conflicts, helping to elaborate our
findings or initiate a new line of thinking~\citep{Miles2015}.
